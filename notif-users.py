#!/usr/bin/env python3
import argparse
import json
import os

import jinja2

import re2oapi

path =(os.path.dirname(os.path.abspath(__file__)))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Send a mail to the users who's membership are going to expire",
    )
    parser.add_argument('-d', '--dry-run', help='Dry run', action='store_true')
    args = parser.parse_args()

    with open(os.path.join(path,'notif-users.json')) as config_file:
        config = json.load(config_file)

    api_client = re2oapi.Re2oAPIClient(config['re2o']['server'],config['re2o']['user'],config['re2o']['password'],use_tls=False,)

    api_sendmail = re2oapi.ApiSendMail(config['smtp']['server'],config['smtp']['port'],)

    users = [ { 'name': user['get_full_name'], 'address': user['get_mail'], 'days': result['days'], }
        for result in api_client.list("reminder/get-users") for user in result["users_to_remind"]
    ]

    with open(os.path.join(path,'templates/notif-users.j2')) as mail_template:
        mail_template = jinja2.Template(mail_template.read())

    for user in users:
        if args.dry_run:
            print('Mail envoyé à {}, reminder {} days'.format(user["name"],user["days"]))
        else:
            api_sendmail.send_mail(
                config['smtp']['from'],
                user['address'],
                "Avis de fin d'adhésion / End of membership notice",
                mail_template.render(user=user),
            )
